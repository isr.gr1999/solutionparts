<?php

namespace App\Http\Controllers;

use App\Models\proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //Almacenamos todas las categorias en la variable $categorias
        $proveedor = Proveedor::all();
        //retornamos la infacion en formato JSON
        return response()->json($proveedor);
    }

    public function store(Request $request)
    {
        $rules = ['proveedor' => 'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }

        $proveedor = new Proveedor($request->input());
        $proveedor->save();

        return response()->json([
            'status' => true,
            'message' => 'Se registro un nuevo proveedor'
        ], 200);
    }


    
    public function show(proveedor $proveedor)
    {
        return response()->json(['status' => true, 'data' => $proveedor]);
    }

    
    public function update(Request $request, proveedor $proveedor)
    {
        //Para guardar y validar que el campo es requerido
        $rules = ['proveedor' => 'required|string|min:1|max:100'];
         //Creamos una nueva varible que crea una nueva validacion en los inputs
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $proveedor->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Proveedor actualizado'
        ],200);
    }

    
    public function destroy(proveedor $proveedor)
    {
        $proveedor->delete();
        return response()->json([
            'status' => true, 
            'message' => 'Proveedor Eliminado'
        ],200);
    }
}
