<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\pedido>
 */
class PedidoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'producto_id' => $this->faker->numberBetween(1,10),
            'cliente_id' => $this->faker->numberBetween(1,10),
            'total' => $this->faker->random_int(1,5000),
            'telefonoPedidos' => $this->faker->e164PhoneNumber
        ];
    }
}
