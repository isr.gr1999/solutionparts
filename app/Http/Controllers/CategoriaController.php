<?php

namespace App\Http\Controllers;


use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CategoriaController extends Controller
{
    
    public function index()
    {
        //Almacenamos todas las categorias en la variable $categorias
        $categorias = Categoria::all();
        //retornamos la informacion en formato JSON
        return response()->json($categorias);

    }

    public function store(Request $request)
    {
        $rules = ['categoria'=>'required|string|min:1|max:100'];
        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }

        $categorias = new Categoria($request->input());
        $categorias->save();

        return response()->json([
            'status' => true,
            'message' => 'Se creó una nueva categoría'
        ], 200);
    }

    public function show(categoria $categoria)
    {
        return response()->json(['status' => true, 'data' => $categoria]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, categoria $categoria)
    {
         //Para guardar y validar que el campo es requerido
        $rules = ['categoria' => 'required|string|min:1|max:100'];
         //Creamos una nueva varible que crea una nueva validacion en los inputs
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $categoria->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Categoria Actualizada'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(categoria $categoria)
    {
        $categoria->delete();
        return response()->json([
            'status' => true, 
            'message' => 'Categoria Eliminada'
        ],200);
    }
}
