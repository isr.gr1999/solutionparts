<?php

namespace App\Http\Controllers;

use App\Models\cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{
    
    public function index()
    {
        //Almacenamos todas las categorias en la variable $categorias
        $cliente = Cliente::all();
        //retornamos la infacion en formato JSON
        return response()->json($cliente);
    }


    public function store(Request $request)
    {
        $rules = ['nombreCliente' => 'required|string|min:1|max:100',
        'correo' => 'required|string|min:1|max:100',
        'telefono' => 'required|numeric'];
        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }
        $cliente = new Cliente($request->input());
        $cliente->save();
        return response()->json([
            'status' => true,
            'message' => 'Se creo un nuevo cliente'
        ], 200);
    }


    public function show(cliente $cliente)
    {
        return response()->json(['status' => true, 'data' => $cliente]);
    }

    
    public function update(Request $request, cliente $cliente)
    {
        $rules = ['nombreCliente' => 'required|string|min:1|max:100',
        'correo' => 'required|string|min:1|max:100',
        'telefono' => 'required|numeric'];
        $validator = Validator::make($request->input(), $rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $cliente ->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Cliente Actualizado'
        ],200);
    }


    public function destroy(cliente $cliente)
    {
        $cliente->delete();
        return response()->json([
            'status' => true, 
            'message' => 'Cliente Eliminado'
        ],200);
    }
}
