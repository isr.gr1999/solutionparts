<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\producto>
 */
class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombreProducto' => $this->faker->word,
            'proveedor_id' =>$this->faker->numberBetween(1,10),
            'categoria_id' =>$this->faker->numberBetween(1,10),
            'precio' => $this->faker->numberBetween(300,10000)
        ];
    }
}
