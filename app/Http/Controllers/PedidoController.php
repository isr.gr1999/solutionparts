<?php

namespace App\Http\Controllers;
use App\Models\cliente;
use App\Models\producto;
use App\Models\pedido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PedidoController extends Controller
{

    public function index()
    {
        $pedido = Pedido::select('pedidos.*','productos.name as productos', 'clientes.name as clientes')
    ->join('productos', 'pedidos.producto_id', '=', 'productos.id')
    ->join('clientes', 'pedidos.cliente_id', '=', 'clientes.id')
    ->paginate(10);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'producto_id' => 'required|numeric',
            'cliente_id' => 'required|numeric',
            'total' => 'required|numeric',
            'telefonoPedidos' => 'required|numeric',
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $pedido = new Pedido($request->input());
        $pedido->save();
        return response()->json([
            'status' => true,
            'message' => 'Pedido Creado',
        ],200);
    }

    
    public function show(pedido $pedido)
    {
        return response()->json([
            'status' => true,
            'data' => $pedido
        ]);
    }

    
    public function update(Request $request, pedido $pedido)
    {
        $rules = [
            'producto_id' => 'required|numeric',
            'cliente_id' => 'required|numeric',
            'total' => 'required|numeric',
            'telefonoPedidos' => 'required|numeric',
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $pedido->update();
        return response()->json([
            'status' => true,
            'message' => 'Pedido Actualizado',
        ],200);
    }

    
    public function destroy(pedido $pedido)
    {
        $pedido->delete();
        return response()->json([
            'status' => true,
            'message' => 'Pedido  Eliminado'
        ],200);
    }

    public function PedidosByCliente(){
        $pedido = Pedido::select(DB::raw('count(pedido.id) as pedido', 'cliente.name'))->join('cliente','cliente.id','=','pedido.cliente_id')->groupBy('cliente.name')->get();
        return response()->json($pedido);
    }
    public function PedidosByProducto(){
        $pedido = Pedido::select(DB::raw('count(pedido.id) as pedido', 'producto.name'))->join('producto','producto.id','=','pedido.producto_id')->groupBy('producto.name')->get();
        return response()->json($pedido);
    }

    public function all(){
        $pedido = Pedido::select('pedido.*','producto.name as producto', 'cliente.name as cliente')
    ->join('producto', 'pedidos.producto_id', '=', 'producto.id')
    ->join('cliente', 'pedidos.cliente_id', '=', 'cliente.id');
    }
}
