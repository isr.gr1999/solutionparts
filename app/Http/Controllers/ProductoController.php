<?php

namespace App\Http\Controllers;

use App\Models\proveedor;
use App\Models\categoria;
use App\Models\producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductoController extends Controller
{
    
    public function index()
    {
        //Traemos toda la informacion de los productos
        $producto = Producto::select('productos.*', 'proveedors.name as provedor', 'categorias.name as categoria')
        ->join('proveedors', 'productos.proveedor_id', '=', 'proveedors.id')
        ->join('categorias', 'productos.categoria_id', '=',   'categorias.id')
        ->paginate(10);

        return response()->json($producto);
    }

    public function store(Request $request)
    {
        $rules = [
            'nombreProducto' => 'required|string|min:1|max:100',
            'proveedor_id' => 'required|numeric',
            'categoria_id' => 'required|numeric',
            'precio' => 'required|numeric',
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto = new Producto($request->input());
        $producto->save();
        return response()->json([
            'status' => true,
            'message' => 'Producto creado',
        ],200);
    }

    public function show(producto $producto)
    {
        return response()->json([
            'status' => true,
            'data' => $producto
        ]); 
    }

    public function update(Request $request, producto $producto)
    {
        $rules = [
            'nombreProducto' => 'required|string|min:1|max:100',
            'proveedor_id' => 'required|numeric',
            'categoria_id' => 'required|numeric',
            'precio' => 'required|numeric',
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $producto->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Producto actualizado'
        ],200);
    }

    public function destroy(producto $producto)
    {
        $producto->delete();
        return response()->json([
            'status' => true,
            'message' => 'Producto  Eliminado'
        ],200);
    }

    public function ProductosByCategoria(){
        //conteo de cuantos productos hay por categoria
        $producto = Producto::select(DB::raw('count(productos.id) as cuenta', 'categorias.name'))->join('categorias','categorias.id','=','productos.categorias_id')->groupBy('categorias.name')->get();
        return response()->json($producto);
    }
    public function ProductosByProveedor(){
        //conteo de cuantos productos hay por categoria
        $producto = Producto::select(DB::raw('count(productos.id) as cuenta', 'proveedors.name'))->join('proveedors','proveedors.id','=','productos.proveedor_id')->groupBy('proveedors.name')->get();
        return response()->json($producto);
    }
    public function all(){
        $producto = Producto::select('producto.*','proveedor.name as provedor', 'categoria.name as categoria')->join('proveedor','proveedor.id')->join('categoria','categoria.id', '=','producto.proveedor_id.categoria_id');
        return response()->json($producto);
    }
}
